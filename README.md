This is a test from Manomano to validate the technical skills in automating an E2E scenario during recruiting phase.

This automation is been done with Cypress.
In order to execute those tests is requiered Node.js 12, 14 or above.
Remember to **npm install** in order to get all the requiered dependencies for the project.

This project includes six different scenarios for the Search functionality.
The different tests are stored at *cypress\integration\search.test.js*

To improve the maintainability of the project every test file will include as many logic as possible. Every kind of logic should be written into support folder. For this example, as we only have Search, we have *cypress\support\search.js* which includes the different methods we will use to run the Search scenarios.

On *cypress.json* we can define different variables deppending on the environment we work. As currently we only work with production, I only defined custom values for production.

Tests can be executed headless or GUI.

To execute tests in headless mode you can use the commander line to run: **npm run cy:run**
To execute tests in GUI mode you can use the commander line to open cypress: **npm run cy:open**

# GUI MODE

In GUI mode the Cypress application opens.


![alt text](img/cypress.jpg "Cypress")


And you can see all the Test Suites for the project.
In this case we only have Search.
If you click to run, you will see the different tests that are attached to this test suite running with their logs and see the execution in real time.

![alt text](img/testStarts.jpg "Execution of Test Suite")


### Example of a test passing fine:


![alt text](img/testOk.jpg "A Test has passed!")


### Example of a test failing:


![alt text](img/testFails.jpg "A Test has failed!")


Once all the test are run you can check the execution and their logs.


![alt text](img/allTestRuns.jpg "All results can be checked after execution is finished")


# HEADLESS MODE

This is the best mode if want to integrate with pipelines since is faster and doesn't requiere a visual interface.


![alt text](img/headless.jpg "Cypress in Headless mode")


If test fails, an image and a video will be stored in order to be able to check what happened during the execution of the test.
Image will be stored at cypress\screenshots\search.test.js\NameOfTheTest.png
Videos will be stored at cypress\videos\NameofTestSuite.mp4


![Video of test failing](cypress/videos/search.test.js.mp4)


I let an image and video in the repository in order to check as sample how the image and video are.
