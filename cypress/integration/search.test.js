context('Search Non Registered User', () => {
    beforeEach(() => {
      cy.visit(Cypress.env('baseURL'));
      try{
      cy.get('#CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll').click();
      }
      catch(e){cy.log('Cookie bot dialog has not appeared')}
    })
    
    it('SE-NRU-01 - Search for a product', () => {
        cy.search(Cypress.env('product'));
        expect(cy.contains(Cypress.env('product')))
        cy.get('[data-testid="information-line"]').should('exist');
        cy.url().should('include', '/search/'+Cypress.env('product'))
    })
    it('SE-NRU-02 - Search for a product pressing INTRO button', () => {
        cy.searchWithIntro(Cypress.env('product'));
        expect(cy.contains(Cypress.env('product')))
        cy.get('[data-testid="information-line"]').should('exist');
        cy.url().should('include', '/search/'+Cypress.env('product'))
    })
    it('SE-NRU-03 - Search for a brand', () => {
        cy.search('Plumbworld');
        expect(cy.contains('Plumbworld'))
        cy.get('[data-testid="information-line"]').should('exist');
        cy.url().should('include', '/search/Plumbworld')
    })
    it('SE-NRU-04 - Suggested products must be shown', () => {
        cy.inputOnSearch(Cypress.env('product'));
        expect(cy.contains(Cypress.env('product')));
        cy.get('[href="/search/'+Cypress.env('product')+'"]').should('have.text', Cypress.env('product'))
    })
    it('SE-NRU-05 - Empty search should show nothing', () => {
        cy.get('#submit-form').click();
        cy.url().should('equal', Cypress.env('baseURL')+'/')
    })
    it('SE-NRU-06 - Search wit special characters', () => {
        cy.search('́|@#|€@#~(/%&28A!’');
        cy.get('[href="/search/|@#|€@#~(/%&28A!’"]').should('have.text', '|@#|€@#~(/%&28A!’')
    })
})