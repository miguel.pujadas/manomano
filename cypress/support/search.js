Cypress.Commands.add("inputOnSearch", (value) => { 
    cy.get('#searchbar').click()
    .type(value)
    .should('have.attr','value',value)
})
Cypress.Commands.add("search", (value) => { 
    cy.inputOnSearch(value);
    cy.get('#submit-form').click()
})
Cypress.Commands.add("searchWithIntro", (value) => { 
    cy.inputOnSearch(value)
    .type('{enter}');
})
